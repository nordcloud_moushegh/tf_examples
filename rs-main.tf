###########################
# Security group
###########################
module "sg" {
  source  = "modules/terraform-aws-security-group"
  
  name   = var.redshift_sg_name
  vpc_id = var.vpc_id

  # Allow ingress rules to be accessed only within current VPC
  ingress_cidr_blocks = [var.vpc_cidr_block]

  # Allow all rules for all protocols
  egress_rules = ["all-all"]
}

###########
# Redshift
###########
module "redshift" {
  source = "modules/terraform-aws-redshift/"

  cluster_identifier      = var.cluster_identifier
  cluster_node_type       = var.cluster_node_type
  cluster_number_of_nodes = var.cluster_number_of_nodes

  cluster_database_name   = var.cluster_database_name
  cluster_master_username = var.cluster_master_username
  cluster_master_password = var.cluster_master_password

  subnets                 = var.vpc_subnets 
  vpc_security_group_ids = [module.sg.this_security_group_id]
  tags  = var.tags
}

#####DYNAMO####
module "dynamodb_table" {
  source                       = "modules/terraform-aws-dynamodb"
  namespace                    = var.namespace
  stage                        = "dev"
  name                         = "cluster"
  hash_key                     = "HashKey"
  range_key                    = "RangeKey"
  autoscale_write_target       = 50
  autoscale_read_target        = 50
  autoscale_min_read_capacity  = 5
  autoscale_max_read_capacity  = 20
  autoscale_min_write_capacity = 5
  autoscale_max_write_capacity = 20
  enable_autoscaler            = true
}