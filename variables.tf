variable vpc_id {
  description = "vpc where the Redshift custer is deployed"
  default = ""
  type = string
}
variable vpc_cidr_block {
  description = "CIDR block for SG"
  default = ""
  type = string
}
variable vpc_subnets {
  type = list
  default = []
  description = "list of subnets where cluster should be deployed"
}

variable redshift_sg_name {
  type = string
  default = ""
  description = "the name of sg for redshift"

}
variable tags {
    type = map
    default = {}
    description = "map of tags for redshift"
}

variable cluster_identifier {
  description = "cluster identifier"
  default = ""
  type = string
}

variable cluster_node_type {
  description = "cluste node type"
  default = ""
  type = string 
}

variable cluster_node_type {
  description = "cluste node type"
  default = ""
  type = string 
}

variable cluster_number_of_nodes {
  description = "number of nodes in cluster"
  default = 1
  type = string 
}

variable cluster_database_name {
  description = "database name for cluster"
  default = ""
  type = string 
}
variable cluster_master_username {
  description = "database master username for cluster"
  default = ""
  type = string 
}
variable cluster_master_password {
  description = "database master username for cluster"
  default = ""
  type = string 
}

variable namespace {
    description = "namespace for dynamodb"
    default = ""
    type = 
}